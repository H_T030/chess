package chess.engine;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Specific list for the storage of chess pieces
 *
 * @author Harrison Towell
 * @version 1.0
 */
public class ChessList implements Iterable<Chessman> {
    private final List<Chessman> allUnits = new ArrayList<>();

    void add(Chessman unit) {
        this.allUnits.add(unit);
    }

    void clear() {
        this.allUnits.clear();
    }

    void removeUnitByPosition(Pos pos) {
        Chessman toBeRemoved = null;
        for (Chessman unit : this.allUnits) {
            if (unit.getPosition().equals(pos)) {
                toBeRemoved = unit;
            }
        }
        allUnits.remove(toBeRemoved);
    }

    /**
     * Checks a position within the list to see if it exists
     *
     * @param pos position to check
     * @return unit at position
     */
    public boolean checkPosition(Pos pos) {
        for (Chessman unit : this.allUnits) {
            if (unit.getPosition().equals(pos)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retrieves a unit at a position, throwing an error if there isn't one
     *
     * @param pos position to retrieve from
     * @return unit at position
     */
    public Chessman getByPos(Pos pos) {
        for (Chessman unit : this.allUnits) {
            if (unit.getPosition().equals(pos)) {
                return unit;
            }
        }
        throw new NullPointerException("Unit doesn't exist at position: " + pos);
    }

    List<Chessman> getByOwner(Player owner) {
        List<Chessman> ownerUnits = new ArrayList<>();
        for (Chessman unit : this.allUnits) {
            if (unit.getOwner().equals(owner)) {
                ownerUnits.add(unit);
            }
        }
        return ownerUnits;
    }

    Chessman getKing(Player owner) {
        for (Chessman unit : this.allUnits) {
            if (unit.getUnitType().equals(Chessman.UnitType.KING) && unit.getOwner().equals(owner)) {
                return unit;
            }
        }
        throw new NullPointerException("No " + owner.getName() + " King exists");
    }

    @Override
    public Iterator<Chessman> iterator() {
        return allUnits.iterator();
    }
}
