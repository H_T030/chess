package chess.engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Chess Game Class used for playing a game of chess
 *
 * @author Harrison Towell
 * @version 1.0
 */
public class Board {
    private static final String ALPHABET = "ABCDEFGH";
    private ChessList allUnits = new ChessList();
    private Pawn pawnPromotion = null;
    private Player checkedPlayer = null;
    private String lastMove = null;

    /**
     * Initialises the board with units
     *
     * @param pawns Defines whether pawns should be created
     */
    public Board(boolean pawns) {
        int y = 1, a = 1;
        allUnits.add(new King(Player.WHITE, new Pos(5, 1), this, false));
        allUnits.add(new King(Player.BLACK, new Pos(5, 8), this, false));
        for (Player o : Player.values()) {
            allUnits.add(new Rook(o, new Pos(1, y), this, false));
            allUnits.add(new Rook(o, new Pos(8, y), this, false));
            allUnits.add(new Knight(o, new Pos(2, y), this, false));
            allUnits.add(new Knight(o, new Pos(7, y), this, false));
            allUnits.add(new Bishop(o, new Pos(3, y), this, false));
            allUnits.add(new Bishop(o, new Pos(6, y), this, false));
            allUnits.add(new Queen(o, new Pos(4, y), this, false));
            if (pawns) {
                for (int i = 1; i < 9; i++) {
                    allUnits.add(new Pawn(o, new Pos(i, y + a), this, false));
                }
            }
            y = 8;
            a = -1;
        }
    }

    private Board(ChessList allUnits) {
        for (Chessman unit : allUnits) {
            this.allUnits.add(unit.copy(this));
        }
    }

    /**
     * Checks for checkmate by testing every possible move of every unit which
     * could possibly prevent the checkmate by moving or attacking. Creates a new board
     * for every move in case the move deletes a unit, as there would be no way to recreate
     * that unit.
     *
     * @param player the player to check for checkmate
     * @return true if player is in checkmate
     */
    boolean checkMate(Player player) {
        for (Chessman unit : this.allUnits.getByOwner(player)) {
            for (Pos move : unit.getPossibleMoves()) {
                Board t = new Board(this.allUnits);
                t.moveUnit(unit.getPosition(), move, unit.getOwner());
                if (!t.check(player)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Checks for check by checking every units possible moves. Its impossible for a units
     * possible moves to contain one of the same owner so there is no need to check if the
     * unit is an enemy
     *
     * @param player the player to check for check
     * @return true if player is in check
     */
    boolean check(Player player) {
        for (Chessman unit : allUnits) {
            if (unit.getPossibleMoves().contains(allUnits.getKing(player).getPosition())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks for stalemate by attempting every possible move of the player, if no moves are possible
     * its stalemate
     *
     * @param turn current turn of the game
     * @return boolean whether in stalemate or not
     */
    public boolean stalemate(Player turn) {
        for (Chessman unit : this.allUnits.getByOwner(turn)) {
            for (Pos move : unit.getPossibleMoves()) {
                Board t = new Board(this.allUnits);
                if (t.moveUnit(unit.getPosition(), move, unit.getOwner())) {
                    return false;
                }
            }
        }
        return true;
    }

    boolean positionContainsUnit(Pos pos) {
        return this.allUnits.checkPosition(pos);
    }

    void addUnit(Chessman unit) {
        this.allUnits.add(unit);
    }

    void destroyUnit(Pos pos) {
        this.allUnits.removeUnitByPosition(pos);
    }

    /**
     * Saves a copy of the game to a file specified by the user in the format of:
     * Owner + Unit Type + Position + Moved Status
     *
     * @param fileName String supplied by user
     * @param turn     The current turn
     * @return error or success
     */
    public String save(String fileName, String turn) {
        try {
            PrintWriter writer = new PrintWriter(fileName);
            writer.println(turn);
            for (Chessman unit : this.allUnits) {
                String buffer = unit.getOwner().getName() + " ";
                buffer += unit.getUnitType().getDisplay() + " ";
                buffer += unit.getPosition().x + " " +  unit.getPosition().y + " ";
                buffer += unit.getMovedStatus();
                writer.println(buffer);
            }
            writer.close();
        } catch (Exception e) {
            return "Error attempting to save game: " + e.toString();
        }
        return "Game successfully saved as " + fileName;
    }

    /**
     * Loads a previously saved game
     *
     * @param fileName String supplied by user
     * @return error or success
     */
    public String load(String fileName) {
        String turn;
        try {
            File file = new File(fileName);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            allUnits.clear();
            turn = bufferedReader.readLine();
            while ((line = bufferedReader.readLine()) != null) {
                Player owner;
                String[] sections = line.split("\\s+");
                if (sections[0].equals(Player.WHITE.getName())) {
                    owner = Player.WHITE;
                } else {
                    owner = Player.BLACK;
                }

                Pos position = new Pos(Integer.parseInt(sections[2]), Integer.parseInt(sections[3]));
                boolean moved = Boolean.parseBoolean(sections[4]);
                switch (sections[1]) {
                    case "B":
                        allUnits.add(new Bishop(owner, position, this, moved));
                        break;
                    case "K":
                        allUnits.add(new King(owner, position, this, moved));
                        break;
                    case "H":
                        allUnits.add(new Knight(owner, position, this, moved));
                        break;
                    case "P":
                        allUnits.add(new Pawn(owner, position, this, moved));
                        break;
                    case "Q":
                        allUnits.add(new Queen(owner, position, this, moved));
                        break;
                    case "R":
                        allUnits.add(new Rook(owner, position, this, moved));

                }
            }
            bufferedReader.close();

        } catch (Exception e) {
            return "Error attempting to load game: " + e.toString();
        }
        return turn + " Successfully loaded " + fileName;
    }

    /**
     * Performs a legal move for the specified player, attacking anything possible
     * otherwise moving a random piece
     *
     * @param player Player to move unit for
     */
    public void performAutomaticMove(Player player) {
        Player opponent = Player.values()[player.getValue() ^ 1]; // ^ XOR flip
        List<Pos> enemyPositions = new ArrayList<>();
        for (Chessman unit : this.allUnits.getByOwner(opponent)) {
            enemyPositions.add(unit.getPosition());
        }

        List<Chessman> playerUnits = this.allUnits.getByOwner(player);
        Pos origin = null;
        Pos target = null;
        ownerUnits:
        for (Chessman unit : playerUnits) {
            for (Pos pos : enemyPositions) {
                if (unit.getPossibleMoves().contains(pos)) {
                    origin = unit.getPosition();
                    target = pos;
                    break ownerUnits;
                }
            }
        }

        if (origin != null) {
            boolean good = this.moveUnit(origin, target, player);
            if (good) {
                this.aiPawnPromotion();
                return;
            }
        }

        while (true) {
            Collections.shuffle(playerUnits);
            Chessman randomUnit = playerUnits.get(0);
            List<Pos> possibleMoves = randomUnit.getPossibleMoves();
            Collections.shuffle(possibleMoves);
            if (possibleMoves.size() > 0) {
                Pos move = possibleMoves.get(0);
                boolean good = this.moveUnit(randomUnit.getPosition(), move, player);
                if (good) {
                    this.aiPawnPromotion();
                    return;
                }
            }
        }
    }

    private void aiPawnPromotion() {
        if (this.pawnPromotion != null) {
            this.promotePawn(1);
        }
    }

    /**
     * Moves a unit on the board
     * RULES APPLIED:
     * Can't move empty spaces
     * Can't move other players pieces
     *
     * @param origin the position of the piece to be moved
     * @param target the position of where the origin is to go
     * @param owner  the player moving the piece
     * @return whether the move was completed or failed due to a rule
     */
    public boolean moveUnit(Pos origin, Pos target, Player owner) {
        String buffer = owner.getName() + " moves ";
        String cords =
            String.valueOf(ALPHABET.charAt(origin.x - 1)) + (origin.y) + " to " +
            String.valueOf(ALPHABET.charAt(target.x - 1)) + (target.y);

        if (this.positionContainsUnit(origin)) {
            Chessman unit = this.allUnits.getByPos(origin);
            lastMove = buffer + unit.getUnitType().getName() + " " + cords;
            if (unit.getOwner().equals(owner)) {
                return unit.move(target);
            }
        }
        lastMove = buffer + cords;
        return false;
    }

    /**
     * Called from interface once victor has reported there is a pawn for promotion
     * based on whether the variable pawnPromotion is null or not. The interface will
     * prompt the user for input and call this function to promote the pawn.
     *
     * @param t indicates which unit to promote too can only be 1-4
     */
    public void promotePawn(int t) {
        this.destroyUnit(pawnPromotion.getPosition());
        Chessman unit = null;
        Player o = pawnPromotion.getOwner();
        Pos p = pawnPromotion.getPosition();
        switch (t) {
            case 1:
                unit = new Queen(o, p, this, true);
                break;
            case 2:
                unit = new Knight(o, p, this, true);
                break;
            case 3:
                unit = new Rook(o, p, this, true);
                break;
            case 4:
                unit = new Bishop(o, p, this, true);
                break;
        }
        this.addUnit(unit);
        pawnPromotion = null;
    }

    /**
     * Retrieves the ChessList of allUnits used for the game
     *
     * @return ChessList of allUnits
     */
    public ChessList getAllUnits() {
        return this.allUnits;
    }

    void setPawnPromotion(Pawn p) {
        this.pawnPromotion = p;
    }

    /**
     * Returns which player is in check
     *
     * @return Player
     */
    public Player getCheckedPlayer() {
        return this.checkedPlayer;
    }

    /**
     * Returns the last move performed in a readable string form
     * @return String
     */
    public String getLastMove(){
        return this.lastMove;
    }

    /**
     * Used to determine the current status of the game, whether a player is in check/checkmate,
     * a pawn is up for promotion, stalemate or the game is still going.
     *
     * @return game status
     */
    public GameStatus victor() {
        for (Player player : Player.values()) {
            if (this.pawnPromotion != null) {
                return GameStatus.PAWN_PROMOTION;
            }
            if (this.check(player)) {
                this.checkedPlayer = player;
                if (this.checkMate(player)) {
                    return GameStatus.CHECKMATE;
                }
                return GameStatus.CHECK;
            }
        }
        this.checkedPlayer = null;
        return GameStatus.GOOD;
    }


    public enum GameStatus {GOOD, CHECK, PAWN_PROMOTION, CHECKMATE}
}
