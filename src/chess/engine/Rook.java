package chess.engine;

import static chess.engine.Direction.*;

class Rook extends Chessman {
    private static final Direction[] MOVE_DIRECTIONS = {N, E, S, W};
    private static final UnitType UNIT_TYPE = UnitType.ROOK;

    Rook(Player owner, Pos position, Board board, boolean moved) {
        super(owner, board, position, UNIT_TYPE, Chessman.listOfDirections(MOVE_DIRECTIONS), moved);
    }

    @Override
    public Rook copy(Board board) {
        return new Rook(super.getOwner(), super.getPosition().copy(), board, super.getMovedStatus());
    }

    @Override
    public boolean move(Pos target) {
        boolean success = super.doMove(target);
        if (!super.getMovedStatus() && success) {
            super.setMovedStatus();
        }
        return success;
    }
}
