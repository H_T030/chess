package chess.engine;

import static chess.engine.Direction.*;

class Bishop extends Chessman {
    private static final Direction[] MOVE_DIRECTIONS = {NW, NE, SE, SW};
    private static final UnitType UNIT_TYPE = UnitType.BISHOP;

    Bishop(Player owner, Pos position, Board board, boolean moved) {
        super(owner, board, position, UNIT_TYPE, Chessman.listOfDirections(MOVE_DIRECTIONS), moved);
    }

    @Override
    public Bishop copy(Board board) {
        return new Bishop(super.getOwner(), super.getPosition().copy(), board, super.getMovedStatus());
    }

}
