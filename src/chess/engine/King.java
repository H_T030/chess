package chess.engine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static chess.engine.Direction.*;

class King extends Chessman {
    private static final Direction[] MOVE_DIRECTIONS = {NW, N, NE, E, SE, S, SW, W};
    private static final List<Pos> CASTLING_MOVE = new ArrayList<>(Arrays.asList(new Pos(3, 1), new Pos(7, 1), new Pos(3, 8), new Pos(7, 8)));
    private static final List<Pos> ROOKS = new ArrayList<>(Arrays.asList(new Pos(1, 1), new Pos(8, 1), new Pos(1, 8), new Pos(8, 8)));
    private static final UnitType UNIT_TYPE = UnitType.KING;

    King(Player owner, Pos position, Board board, boolean moved) {
        super(owner, board, position, UNIT_TYPE, Chessman.listOfDirections(MOVE_DIRECTIONS), moved);
    }

    @Override
    List<Pos> getPossibleMoves() {
        return super.calculateMoves(false);
    }

    @Override
    public King copy(Board board) {
        return new King(super.getOwner(), super.getPosition().copy(), board, super.getMovedStatus());
    }

    @Override
    public boolean move(Pos target) {
        int ownerIndex = super.getOwner().getValue() * 2;
        boolean success = false;
        if (CASTLING_MOVE.contains(target) && !super.getMovedStatus()) {
            if (target.equals(CASTLING_MOVE.get(ownerIndex + 1))) { // Castling Short
                int castlingShort = 1;
                success = castlingMove(ownerIndex, castlingShort);
            } else if (target.equals(CASTLING_MOVE.get(ownerIndex))) { // Castling Long
                int castlingLong = 0;
                success = castlingMove(ownerIndex, castlingLong);
            }
        } else {
            success = super.doMove(target);
        }
        if (success) {
            super.setMovedStatus();
        }
        return success;
    }

    /**
     * Performs a king rook swap
     * RULES APPLIED:
     * The king and the rook have not moved
     * There are no pieces between the king and the rook
     * The king is not in check
     * The king does not pass through a square that can be attacked
     * The king does not end up in check
     *
     * @param ownerIndex index based on owner used to retrieve positions from lists
     * @param moveType   Short or Long
     * @return successful
     */
    private boolean castlingMove(int ownerIndex, int moveType) {
        Board board = super.getBoard();
        if (board.positionContainsUnit(ROOKS.get(ownerIndex))) { // Checks if there is a unit in the rook position
            Chessman piece = board.getAllUnits().getByPos(ROOKS.get(ownerIndex + moveType));
            if (piece.getUnitType().equals(UnitType.ROOK)) {
                Rook rook = (Rook) piece;
                if (!rook.getMovedStatus()) { // Makes sure rook hasn't moved
                    if (board.check(super.getOwner())) {
                        return false; // cant castle out of check
                    }
                    Player opponent = Player.values()[super.getOwner().getValue() ^ 1]; // ^ XOR flip

                    // Makes sure that movements aren't through check and that there are empty places between
                    // the king and rook
                    int minX, maxX, rookX;
                    int kingY = super.getPosition().y;
                    if (moveType == 0) {
                        minX = 2; // Castling long
                        maxX = 5;
                        rookX = 4;
                    } else {
                        minX = 6; // Castling short
                        maxX = 8;
                        rookX = 6;
                    }
                    for (Chessman unit : board.getAllUnits().getByOwner(opponent)) {
                        int count = minX;
                        while (count < maxX) {
                            Pos newPos = new Pos(minX, kingY);
                            if (unit.getPossibleMoves().contains(newPos)) {
                                return false;
                            }
                            if (board.positionContainsUnit(newPos)) {
                                return false;
                            }
                            count++;
                        }
                    }

                    super.setPosition(CASTLING_MOVE.get(ownerIndex + moveType));
                    rook.setPosition(new Pos(rookX, kingY));
                    return true;
                }
            }
        }

        return false;
    }

}
