package chess.engine;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit class which all chess pieces extend from.
 *
 * @author Harrison Towell
 * @version 1.0
 */
public abstract class Chessman {
    private final Player owner;
    private final UnitType unitType;
    private final Board board;
    private List<Pos> moveDirections = new ArrayList<>();
    private Pos position;
    private boolean moved;

    Chessman(Player owner, Board board, Pos position, UnitType type, List<Pos> moveDirections, boolean moved) {
        this.owner = owner;
        this.board = board;
        this.position = position;
        this.moveDirections = moveDirections;
        this.unitType = type;
        this.moved = moved;
    }

    /**
     * Converts an array of enum Directions into a list of points
     *
     * @param dirs array of enum Directions
     * @return List of points
     */
    static List<Pos> listOfDirections(Direction[] dirs) {
        List<Pos> p = new ArrayList<>();
        for (Direction d : dirs) {
            p.add(d.getDir());
        }
        return p;
    }

    /**
     * Calculates all possible moves for a unit by adding its movement directions to its position
     * RULES APPLIED:
     * Cant move outside boundaries
     * Cant move through units
     *
     * @param iterate Determines whether the movement directions should be added more than once
     * @return a list of possible moves
     */
    List<Pos> calculateMoves(boolean iterate) {
        List<Pos> calculatedMoves = new ArrayList<>();
        for (Pos move : this.moveDirections) {
            Pos newPos = this.position.copy(); // Breaks the reference
            do {
                newPos.translate(move.x, move.y); // Adjusts position by available movements

                if (newPos.x > 8 || newPos.x < 1 || newPos.y > 8 || newPos.y < 1) {
                    break; // Check out of boundaries
                }

                if (this.board.positionContainsUnit(newPos)) { // Detect collision with other units
                    if (this.board.getAllUnits().getByPos(newPos).getOwner() != this.owner) { // Checks if collision is enemy
                        calculatedMoves.add(newPos.copy()); // If enemy allow move
                    }
                    break; // Can't move in this direction anymore either due to enemy or friendly
                }
                calculatedMoves.add(newPos.copy()); // Breaks reference and adds to possible moves
            } while (iterate); // Depending on piece iterates to generate more moves
        }
        return calculatedMoves;
    }

    /**
     * Does the actual move operation, separated from move, so move can be overridden for
     * special rules of other classes
     * RULES APPLIED:
     * Piece is allowed to move
     * King can't move into check
     * King cant be left in check from move
     *
     * @param target position chosen by player
     * @return successful
     */
    boolean doMove(Pos target) {
        if (this.getPossibleMoves().contains(target)) { // Checks if players move is allowed
            Chessman occupier = null;
            // Destroys a unit if its in the way but keeps a ref as backup
            if (this.board.positionContainsUnit(target)) {
                occupier = this.board.getAllUnits().getByPos(target);
                this.board.destroyUnit(target);
            }

            // Prevents king moving into check
            if (this.getUnitType().equals(UnitType.KING)) {
                for (Chessman unit : this.board.getAllUnits()) {
                    if (unit.getOwner() != this.owner && unit.getPossibleMoves().contains(target)) {
                        if (occupier != null) {
                            this.board.addUnit(occupier);
                        }
                        return false;
                    }
                }
            }

            // Prevents a unit leaving the king in check
            Pos tempPos = this.position.copy();
            this.position = target;
            if (this.board.check(this.owner)) {
                this.position = tempPos;
                if (occupier != null) {
                    this.board.addUnit(occupier);
                }
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Clones the unit
     *
     * @param board new board the piece will be attached to
     * @return copy of current unit on new board
     */
    abstract public Chessman copy(Board board);

    /**
     * Moves this unit
     *
     * @param target position to move to
     * @return successful
     */
    public boolean move(Pos target) {
        return doMove(target);
    }

    List<Pos> getMoveDirections() {
        return moveDirections;
    }

    /**
     * Owner of this unit
     *
     * @return Player
     */
    public Player getOwner() {
        return this.owner;
    }

    /**
     * The type of unit
     *
     * @return UnitType
     */
    public UnitType getUnitType() {
        return this.unitType;
    }

    List<Pos> getPossibleMoves() {
        return this.calculateMoves(true);
    }

    /**
     * Position of the unit
     * @return Pos
     */
    public Pos getPosition() {
        return position;
    }

    void setPosition(Pos position) {
        this.position = position;
    }

    Board getBoard() {
        return this.board;
    }

    boolean getMovedStatus() {
        return moved;
    }

    void setMovedStatus() {
        this.moved = true;
    }

    public enum UnitType {
        BISHOP('B', "Bishop"),
        KING('K', "King"),
        KNIGHT('H', "Knight"),
        PAWN('P', "Pawn"),
        QUEEN('Q', "Queen"),
        ROOK('R', "Rook");
        private final char display;
        private final String name;

        UnitType(char c, String n) {
            this.display = c;
            this.name = n;
        }

        /**
         * Gets the full name of a unit
         * @return String
         */
        public String getName(){
            return this.name;
        }

        /**
         * Gets a text representation of the unit
         *
         * @return text representation
         */
        public char getDisplay() {
            return display;
        }
    }

}
