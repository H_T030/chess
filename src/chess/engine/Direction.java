package chess.engine;

/**
 * Holds move directions for unit setup
 */
enum Direction {
    NW(new Pos(-1, 1)),
    N(new Pos(0, 1)),
    NE(new Pos(1, 1)),
    E(new Pos(1, 0)),
    SE(new Pos(1, -1)),
    S(new Pos(0, -1)),
    SW(new Pos(-1, -1)),
    W(new Pos(-1, 0)),
    N2(new Pos(0, 2)),
    S2(new Pos(0, -2));

    private final Pos dir;

    Direction(Pos dir) {
        this.dir = dir;
    }

    Pos getDir() {
        return dir;
    }
}
