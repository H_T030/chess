package chess.engine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Knight extends Chessman {
    private static final List<Pos> MOVE_DIRECTIONS = new ArrayList<>(Arrays.asList(new Pos(1, 2), new Pos(-1, 2),
            new Pos(2, 1), new Pos(2, -1), new Pos(-2, 1), new Pos(-2, -1), new Pos(-1, -2), new Pos(1, -2)));
    private static final UnitType UNIT_TYPE = UnitType.KNIGHT;

    Knight(Player owner, Pos position, Board board, boolean moved) {
        super(owner, board, position, UNIT_TYPE, MOVE_DIRECTIONS, moved);
    }

    @Override
    List<Pos> getPossibleMoves() {
        return this.calculateMoves(false);
    }

    @Override
    public Knight copy(Board board) {
        return new Knight(super.getOwner(), super.getPosition().copy(), board, super.getMovedStatus());
    }

}
