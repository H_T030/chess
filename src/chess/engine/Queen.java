package chess.engine;

import static chess.engine.Direction.*;

class Queen extends Chessman {
    private static final Direction[] MOVE_DIRECTIONS = {NW, N, NE, E, SE, S, SW, W};
    private static final UnitType UNIT_TYPE = UnitType.QUEEN;

    Queen(Player owner, Pos position, Board board, boolean moved) {
        super(owner, board, position, UNIT_TYPE, Chessman.listOfDirections(MOVE_DIRECTIONS), moved);
    }

    @Override
    public Queen copy(Board board) {
        return new Queen(super.getOwner(), super.getPosition().copy(), board, super.getMovedStatus());
    }

}
