package chess.engine;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the Board class methods and functions
 *
 * @author Harrison Towell
 * @version 1.0
 */
public class BoardTest {

    private Board game;
    private Board noPawns;

    private void doMoves(int[][] moves, boolean pawns) {
        for (int[] m : moves) {
            if (pawns) {
                Player p = game.getAllUnits().getByPos(new Pos(m[0], m[1])).getOwner();
                assertTrue(game.moveUnit(new Pos(m[0], m[1]), new Pos(m[2], m[3]), p));
            } else {
                Player p = noPawns.getAllUnits().getByPos(new Pos(m[0], m[1])).getOwner();
                assertTrue(noPawns.moveUnit(new Pos(m[0], m[1]), new Pos(m[2], m[3]), p));
            }
        }
    }

    /**
     * Creates two boards, with and without pawns
     */
    @Before
    public void setUp() {
        game = new Board(true);
        noPawns = new Board(false);
    }

    /**
     * Tests illegal moves
     */
    @Test
    public void testMoves() {
        assertFalse(game.moveUnit(new Pos(5, 5), new Pos(6, 6), Player.WHITE)); // Move empty space
        assertFalse(game.moveUnit(new Pos(1, 1), new Pos(1, -1), Player.WHITE)); // Move outside bounds
    }

    /**
     * Tests the movements of the rook
     */
    @Test
    public void testRook() {
        assertTrue(noPawns.moveUnit(new Pos(1, 1), new Pos(1, 5), Player.WHITE)); // Straight move
        assertFalse(noPawns.moveUnit(new Pos(1, 5), new Pos(2, 6), Player.WHITE)); // Diagonal move
    }

    /**
     * Test the knights movements
     */
    @Test
    public void testKnight() {
        assertTrue(game.moveUnit(new Pos(2, 1), new Pos(1, 3), Player.WHITE)); // Move in a L shape, jumping units
        assertFalse(game.moveUnit(new Pos(2, 1), new Pos(2, 3), Player.WHITE)); // Move straight

    }

    /**
     * Test the bishops movements
     */
    @Test
    public void testBishop() {
        assertTrue(noPawns.moveUnit(new Pos(4, 1), new Pos(3, 2), Player.WHITE)); // Diagonal move
        assertFalse(noPawns.moveUnit(new Pos(4, 1), new Pos(4, 5), Player.WHITE)); // Straight move
    }

    /**
     * Tests the pawns movements, as well as its ability to attack, not jump units,
     * not move backwards and move 2 spaces
     */
    @Test
    public void testPawn() {
        assertTrue(game.moveUnit(new Pos(1, 2), new Pos(1, 3), Player.WHITE)); // Move 1 space
        assertFalse(game.moveUnit(new Pos(1, 3), new Pos(1, 5), Player.WHITE)); // Move 2 space after already moving
        assertTrue(game.moveUnit(new Pos(2, 2), new Pos(2, 4), Player.WHITE)); // Move 2 space
        game.moveUnit(new Pos(2, 7), new Pos(2, 5), Player.BLACK);
        assertFalse(game.moveUnit(new Pos(2, 4), new Pos(2, 5), Player.WHITE)); // Attack forward 1
        assertFalse(game.moveUnit(new Pos(2, 4), new Pos(2, 3), Player.WHITE)); // Move backwards
        game.moveUnit(new Pos(3, 7), new Pos(3, 5), Player.BLACK);
        game.moveUnit(new Pos(3, 5), new Pos(3, 4), Player.BLACK);
        game.moveUnit(new Pos(3, 4), new Pos(3, 3), Player.BLACK);
        assertFalse(game.moveUnit(new Pos(3, 2), new Pos(3, 4), Player.WHITE)); // Jump over unit with 2 space move
        assertTrue(game.moveUnit(new Pos(3, 3), new Pos(4, 2), Player.BLACK)); // Attack diagonally
        assertFalse(game.moveUnit(new Pos(5, 7), new Pos(4, 6), Player.BLACK)); // Move diagonally without attacking
    }

    /**
     * Tests all 4 variants of a pawn promotion
     */
    @Test
    public void testPawnPromotion() {
        assertTrue(game.victor().equals(Board.GameStatus.GOOD));
        this.doMoves(new int[][]{{1, 2, 1, 4}, {1, 4, 1, 5}, {1, 5, 1, 6}, {1, 6, 2, 7}, {2, 7, 3, 8}}, true);
        assertTrue(game.victor().equals(Board.GameStatus.PAWN_PROMOTION));
        game.promotePawn(1);
        assertTrue(game.getAllUnits().getByPos(new Pos(3, 8)) instanceof Queen);
        this.doMoves(new int[][]{{2, 2, 2, 4}, {2, 4, 2, 5}, {2, 5, 2, 6}, {2, 6, 3, 7}, {3, 7, 4, 8}}, true);
        assertTrue(game.victor().equals(Board.GameStatus.PAWN_PROMOTION));
        game.promotePawn(2);
        assertTrue(game.getAllUnits().getByPos(new Pos(4, 8)) instanceof Knight);
        this.doMoves(new int[][]{{4, 2, 4, 4}, {4, 4, 4, 5}, {4, 5, 4, 6}, {4, 6, 5, 7}, {5, 7, 6, 8}}, true);
        assertTrue(game.victor().equals(Board.GameStatus.PAWN_PROMOTION));
        game.promotePawn(3);
        assertTrue(game.getAllUnits().getByPos(new Pos(6, 8)) instanceof Rook);
        this.doMoves(new int[][]{{5, 2, 5, 4}, {5, 4, 5, 5}, {5, 5, 5, 6}, {5, 6, 6, 7}, {6, 7, 7, 8}}, true);
        assertTrue(game.victor().equals(Board.GameStatus.PAWN_PROMOTION));
        game.promotePawn(4);
        assertTrue(game.getAllUnits().getByPos(new Pos(7, 8)) instanceof Bishop);
    }

    /**
     * Tests movement capabilities of a king
     */
    @Test
    public void testKing() {
        assertTrue(noPawns.moveUnit(new Pos(5, 1), new Pos(5, 2), Player.WHITE)); // Move one space
        assertFalse(noPawns.moveUnit(new Pos(5, 1), new Pos(5, 5), Player.WHITE)); // Move more than 1 space
        assertFalse(noPawns.moveUnit(new Pos(5, 8), new Pos(4, 7), Player.BLACK)); // Move into check
    }

    /**
     * Tests that destroying units works
     */
    @Test
    public void testDestroyUnit() {
        game.moveUnit(new Pos(1, 2), new Pos(1, 4), Player.WHITE);
        game.moveUnit(new Pos(1, 4), new Pos(1, 5), Player.WHITE);
        game.moveUnit(new Pos(1, 5), new Pos(1, 6), Player.WHITE);
        game.moveUnit(new Pos(1, 6), new Pos(2, 7), Player.WHITE);
        game.moveUnit(new Pos(2, 7), new Pos(1, 8), Player.WHITE);
        assertFalse(game.positionContainsUnit(new Pos(2, 7)));
    }

    /**
     * Tests that stalemate can happen when a player has no possible moves
     */
    @Test
    public void testStalemate() {
        assertFalse(game.stalemate(Player.BLACK));
        this.doMoves(new int[][]{{3, 2, 3, 4}, {8, 7, 8, 5}, {8, 2, 8, 4}, {1, 7, 1, 5}, {4, 1, 1, 4}, {1, 8, 1, 6}, {1, 4, 1, 5}, {1, 6, 8, 6},
                {1, 5, 3, 7}, {6, 7, 6, 6}, {3, 7, 4, 7}, {5, 8, 6, 7}, {4, 7, 2, 7}, {4, 8, 4, 3}, {2, 7, 2, 8}, {4, 3, 8, 7}, {2, 8, 3, 8}, {6, 7, 7, 6},
                {3, 8, 5, 6}}, true);
        assertTrue(game.stalemate(Player.BLACK));
    }

    /**
     * Tests that both players ability to be put in check
     */
    @Test
    public void testCheck() {
        noPawns.moveUnit(new Pos(4, 1), new Pos(5, 2), Player.WHITE);
        assertTrue(noPawns.check(Player.BLACK));
        noPawns.moveUnit(new Pos(5, 2), new Pos(4, 1), Player.WHITE);
        noPawns.moveUnit(new Pos(4, 8), new Pos(5, 7), Player.BLACK);
        assertTrue(noPawns.check(Player.WHITE));
    }

    /**
     * Test whether is possible to perform a move leaving the king in check
     */
    @Test
    public void testLeaveKingInCheck() {
        noPawns.moveUnit(new Pos(4, 1), new Pos(5, 2), Player.WHITE); // Black king in check
        assertFalse(noPawns.moveUnit(new Pos(1, 7), new Pos(1, 5), Player.BLACK)); // Doesn't stop king being in check
    }

    /**
     * Tests a normal checkmate where the king is in check and cant move
     */
    @Test
    public void testCheckMate() {
        game.moveUnit(new Pos(6, 2), new Pos(6, 3), Player.WHITE);
        game.moveUnit(new Pos(7, 2), new Pos(7, 4), Player.WHITE);
        game.moveUnit(new Pos(5, 7), new Pos(5, 5), Player.BLACK);
        game.moveUnit(new Pos(4, 8), new Pos(8, 4), Player.BLACK);
        assertTrue(game.victor().equals(Board.GameStatus.CHECKMATE));
        assertTrue(game.checkMate(Player.WHITE));
    }

    /**
     * Tests checkmate when a unit is next to the king but is protected by another unit
     */
    @Test
    public void testCheckMate2() {
        game.moveUnit(new Pos(5, 7), new Pos(5, 5), Player.BLACK);
        game.moveUnit(new Pos(6, 8), new Pos(3, 5), Player.BLACK);
        game.moveUnit(new Pos(3, 5), new Pos(6, 2), Player.BLACK);
        assertFalse(game.checkMate(Player.WHITE));
        game.moveUnit(new Pos(3, 7), new Pos(3, 6), Player.BLACK);
        game.moveUnit(new Pos(4, 8), new Pos(2, 6), Player.BLACK);
        assertTrue(game.checkMate(Player.WHITE));

    }

    /**
     * Tests castling short whilst also testing the rules that apply to it such as
     * not being able to move through check
     */
    @Test
    public void testCastlingShort() {
        this.doMoves(new int[][]{{7, 1, 8, 3}, {6, 1, 4, 3}, {4, 8, 5, 7}}, false);
        assertFalse(noPawns.moveUnit(new Pos(5, 1), new Pos(7, 1), Player.WHITE)); // Move out of check with rook swap
        assertTrue(noPawns.moveUnit(new Pos(5, 7), new Pos(6, 7), Player.BLACK));
        assertFalse(noPawns.moveUnit(new Pos(5, 1), new Pos(7, 1), Player.WHITE)); // Move through check
        assertTrue(noPawns.moveUnit(new Pos(6, 7), new Pos(4, 7), Player.BLACK));
        assertTrue(noPawns.moveUnit(new Pos(5, 1), new Pos(7, 1), Player.WHITE)); // Castling Move
        assertTrue(noPawns.getAllUnits().getByPos(new Pos(7, 1)) instanceof King);
        assertTrue(noPawns.getAllUnits().getByPos(new Pos(6, 1)) instanceof Rook);
    }

    /**
     * Tests that castling long works
     */
    @Test
    public void testCastlingLong() {
        this.doMoves(new int[][]{{2, 1, 1, 3}, {3, 1, 5, 3}, {4, 1, 1, 4}}, false);
        assertTrue(noPawns.moveUnit(new Pos(5, 1), new Pos(3, 1), Player.WHITE)); // Castling Move
        assertTrue(noPawns.getAllUnits().getByPos(new Pos(3, 1)) instanceof King);
        assertTrue(noPawns.getAllUnits().getByPos(new Pos(4, 1)) instanceof Rook);
    }

    /**
     * Tests the save and loading of games
     */
    @Test
    public void testSaveLoad() {
        game.moveUnit(new Pos(1, 2), new Pos(1, 4), Player.WHITE);
        game.save("test save.txt", Player.BLACK.getName());
        game = new Board(true);
        assertFalse(game.positionContainsUnit(new Pos(1, 4)));
        game.load("test save.txt");
        assertTrue(game.positionContainsUnit(new Pos(1, 4)));
    }

    /**
     * Tests an AI move by comparing positions between iterations
     */
    @Test
    public void testAutomaticMove() {
        assertTrue(noPawns.getAllUnits().getByOwner(Player.WHITE).size() > 7);
        assertTrue(noPawns.getAllUnits().getByOwner(Player.BLACK).size() > 7);
        for (int i = 0; i < 10; i++) {
            noPawns.performAutomaticMove(Player.WHITE);
            noPawns.performAutomaticMove(Player.BLACK);
        }
        assertTrue(noPawns.getAllUnits().getByOwner(Player.WHITE).size() < 7);
        assertTrue(noPawns.getAllUnits().getByOwner(Player.BLACK).size() < 7);
    }
}

