package chess.engine;

import java.awt.Point;

/**
 * Holder class of positions in X Y format
 *
 * @author Harrison Towell
 * @version 1.0
 */
public class Pos extends Point{

    /**
     * Creates a normal point
     * @param x int
     * @param y int
     */
    public Pos(int x, int y) {
        super.x = x;
        super.y = y;
    }

    Pos copy() {
        return new Pos(super.x, super.y);
    }

}
