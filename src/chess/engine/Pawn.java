package chess.engine;

import java.util.ArrayList;
import java.util.List;

import static chess.engine.Direction.*;

class Pawn extends Chessman {
    private static final Direction[][] moveDirections = {{NW, N, NE, N2}, {SW, S, SE, S2}};
    private static final UnitType UNIT_TYPE = UnitType.PAWN;

    Pawn(Player owner, Pos position, Board board, boolean moved) {
        super(owner, board, position, UNIT_TYPE, Chessman.listOfDirections(moveDirections[owner.getValue()]), moved);
    }

    @Override
    public Pawn copy(Board board) {
        return new Pawn(super.getOwner(), super.getPosition().copy(), board, super.getMovedStatus());
    }

    /**
     * Adjusts pawn movements according to chess rules
     * RULES APPLIED:
     * Pawns can't jump units
     * Pawns can only attack diagonally
     *
     * @return List of possible moves
     */
    @Override
    List<Pos> getPossibleMoves() {
        List<Pos> nonRuleMoves = super.calculateMoves(false);
        List<Pos> allowedMoves = new ArrayList<>();

        for (Pos move : nonRuleMoves) {
            double d = super.getPosition().distance(move);
            boolean occupied = super.getBoard().positionContainsUnit(move);

            if (1 < d && d < 2) { // Checks for diagonal move
                if (!occupied) {
                    continue; // If no enemy, bad move
                }
            } else if (occupied) {
                continue; // Stops pawn taking units in front of it
            }
            if (d == 2) {
                // Stops pawn jumping by checking position it would jump for clearance
                int i = (super.getOwner() == Player.BLACK ? 1 : -1);
                if (super.getBoard().positionContainsUnit(new Pos(move.x, move.y + i))) {
                    continue;
                }
            }
            allowedMoves.add(move);
        }
        return allowedMoves;
    }

    /**
     * Moves a pawn, removing its ability to move 2 spaces after its first move
     * RULES APPLIED:
     * Pawn is promoted on reaching final rank
     * Pawn can only jump 2 spaces on first move
     *
     * @param target position chosen by player
     * @return successful
     */
    @Override
    public boolean move(Pos target) {
        if (super.doMove(target)) {
            if (super.getPosition().y == super.getOwner().getFinalRank()) {
                super.getBoard().setPawnPromotion(this);
            }
            if (!super.getMovedStatus()) {
                super.getMoveDirections().remove(N2.getDir());
                super.getMoveDirections().remove(S2.getDir());
                super.setMovedStatus();
            }

            return true;
        }
        return false;
    }
}
