package chess.engine;

/**
 * Defines players and there values
 *
 * @author Harrison Towell
 * @version 1.0
 */
public enum Player {
    WHITE(0, 8, "White"),
    BLACK(1, 1, "Black");

    private final int value;
    private final int finalRank;
    private final String name;

    Player(int v, int r, String n) {
        this.value = v;
        this.finalRank = r;
        this.name = n;
    }

    /**
     * Gets the value of a player (0,1)
     * @return int
     */
    public int getValue() {
        return this.value;
    }

    int getFinalRank() {
        return finalRank;
    }

    /**
     * String representation of player
     *
     * @return String
     */
    public String getName() {
        return name;
    }
}
