package chess.textui;

import chess.engine.*;

import java.io.*;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Text representation of the chess engine for use in the command line
 *
 * @author Harrison Towell
 * @version 1.0
 */
public class TextUI {
    private static final String ALPHABET = "abcdefgh";
    private static final Pattern MOVE = Pattern.compile("([a-h])([1-8])(\\s+)([a-h])([1-8])", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
    private static final Pattern NUMBER = Pattern.compile("([1-4])");
    private final Scanner in;
    private final PrintStream out;
    private Player aiPlayer = null;

    private TextUI(Scanner input) {
        this.in = input;
        this.out = System.out;
    }

    /**
     * Initialisation of text ui
     *
     * @param args arguments from cmd
     */
    public static void main(String[] args) {
        if (args.length > 2) {
            try {
                File file = new File(args[1]);
                FileReader fileReader = new FileReader(file);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String line = bufferedReader.readLine();
                bufferedReader.close();
                boolean fastGame = false;
                Player ai = null;
                if (line.length() == 3) {
                    if (line.substring(0, 1).equals("y")) {
                        fastGame = true;
                    }
                    if (line.substring(1, 2).equals("y")) {
                        if (line.substring(2, 3).equals("y")) {
                            ai = Player.BLACK;
                        } else {
                            ai = Player.WHITE;
                        }
                    }
                    TextUI ui = new TextUI(new Scanner(System.in));
                    Board game = new Board(fastGame);
                    ui.setAiPlayer(ai);
                    try {
                        PrintWriter writer = new PrintWriter(args[2]);
                        writer.println(ui.display(game));
                        writer.close();
                        System.exit(0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("Bad input file");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            TextUI ui = new TextUI(new Scanner(System.in));
            Board game = new Board(ui.gameSetUp());
            ui.playTextGame(game, ui.aiPlayer);
        }
    }

    private boolean gameSetUp() {
        out.println("Welcome to Chess!");
        out.println("Would you like to play a fast game Y/N?");
        boolean fastGame = scanInput();
        out.println("Would you like to play verse AI Y/N?");
        boolean ai = scanInput();
        if (ai) {
            out.println("Would you like to play as White Y/N?");
            boolean side = scanInput();
            if (side) {
                this.aiPlayer = Player.BLACK;
            } else {
                this.aiPlayer = Player.WHITE;
            }
        }
        return !fastGame;
    }

    private boolean scanInput() {
        while (true) {
            String input = in.nextLine();
            switch (input.toLowerCase()) {
                case "y":
                    return true;
                case "n":
                    return false;
                default:
                    out.println("Bad input");
            }
        }
    }

    private void playTextGame(Board game, Player ai) {
        this.aiPlayer = ai;
        Player turn = Player.WHITE;
        main:
        while (true) {
            if (game.stalemate(turn)) {
                out.println(turn.getName() + " can't move. Stalemate.");
                break;
            }
            out.print(this.display(game));
            out.println(turn.getName() + "'s turn");
            // AI turn if applicable
            if (this.aiPlayer != null && this.aiPlayer.equals(turn)) {
                game.performAutomaticMove(aiPlayer);
                turn = changeTurn(turn);
                switch (game.victor()) {
                    case CHECK:
                        this.check(game);
                        break;
                    case CHECKMATE:
                        this.checkMate(game, turn);
                        break main;
                    default:
                        break;
                }
                continue;
            }
            out.println("Enter your move (e.g. A2 A4): ");
            String input = in.nextLine();
            // Saving and Loading of games
            if (input.substring(0, 4).toLowerCase().equals("save")) {
                String fileName = input.substring(5, input.length());
                out.println(game.save(fileName, turn.getName()));
                continue;
            } else if (input.toLowerCase().substring(0, 4).equals("load")) {
                String success = game.load(input.substring(5, input.length()));
                String currentTurn = success.substring(0, 5);
                if (currentTurn.equals(Player.WHITE.getName())) {
                    turn = Player.WHITE;
                } else {
                    turn = Player.BLACK;
                }
                out.println(success.substring(5, success.length()));
                continue;
            }
            // Playing and movements of games
            Matcher m = MOVE.matcher(input);
            if (m.find()) {
                int ox = ALPHABET.indexOf(m.group(1).toLowerCase()) + 1; // Increment by 1 to remove zero index
                int tx = ALPHABET.indexOf(m.group(4).toLowerCase()) + 1;
                int oy = Integer.parseInt(m.group(2));
                int ty = Integer.parseInt(m.group(5));

                boolean good = game.moveUnit(new Pos(ox, oy), new Pos(tx, ty), turn);
                if (good) {
                    turn = changeTurn(turn);
                    switch (game.victor()) {
                        case CHECK:
                            this.check(game);
                            break;
                        case PAWN_PROMOTION:
                            out.println("Enter a number to promote the pawn to:");
                            out.println("1. Queen, 2. Knight, 3. Rook, 4. Bishop ");
                            while (true) {
                                Matcher m2 = NUMBER.matcher(in.nextLine());
                                if (m2.find()) {
                                    game.promotePawn(Integer.parseInt(m2.group()));
                                    break;
                                } else {
                                    out.println("Bad input");
                                }
                            }
                            break;
                        case CHECKMATE:
                            this.checkMate(game, turn);
                            break main;
                        default:
                            break;
                    }
                } else {
                    out.println("Bad move");
                }
            } else {
                out.println("Bad input");
            }
        }
    }

    private void check(Board game) {
        out.println(game.getCheckedPlayer().getName() + " is in check!");
    }

    private void checkMate(Board game, Player turn) {
        out.print(this.display(game));
        out.println(turn.getName() + " is in checkmate");
    }

    private Player changeTurn(Player turn) {
        if (turn.equals(Player.WHITE)) {
            return Player.BLACK;
        } else {
            return Player.WHITE;
        }
    }


    private void setAiPlayer(Player ai) {
        this.aiPlayer = ai;
    }

    /**
     * Checks every possible position on the board for a piece and draws it, if no piece fills
     * the place with dots to represent an empty place.
     *
     * @return String representation of the board
     */
    private String display(Board game) {
        String output = "";
        ChessList allUnits = game.getAllUnits();
        for (int y = 8; y > 0; y--) { // Iterates the y axis
            output += y + " "; // Appends the appropriate row number to the beginning
            for (int x = 1; x < 9; x++) { // Iterates the x axis
                Pos pos = new Pos(x, y);
                if (allUnits.checkPosition(pos)) { // If a unit exists at the position, draws it
                    Chessman unit = game.getAllUnits().getByPos(pos);
                    output += unit.getOwner().getName().substring(0, 1) + unit.getUnitType().getDisplay() + " ";
                } else { // else fills the place with a blank
                    output += ".. ";
                }
            }
            output += System.lineSeparator();
        }
        return output + "  A  B  C  D  E  F  G  H \n"; // Column letters
    }
}
