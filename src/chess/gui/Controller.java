package chess.gui;


import chess.engine.Board;
import chess.engine.Chessman;
import chess.engine.Player;
import chess.engine.Pos;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.File;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Controller class for the Chess GUI, contains all the code to handle the GUI and
 * act as the middleman between the engine package and the front end
 *
 * @author Harrison Towell
 * @version 1.0
 */
public class Controller {
    private final Map<String, Image> images = new HashMap<>();
    private final Pane[][] panes = new Pane[8][8];
    private final Timeline timer = new Timeline();
    private final int[] playerSeconds = new int[]{0, 0};
    @FXML private GridPane grid;
    @FXML private AnchorPane anchor;
    @FXML private TextArea gameStatusLabel;
    @FXML private SplitPane boardSplitPane;
    @FXML private ToggleButton toggleButtonAI;
    @FXML private Label whiteTimerLabel;
    @FXML private Label blackTimerLabel;
    private Stage stage;
    private Pane selectedPane = null;
    private Pane hoveredPane = null;
    private Board game = new Board(true);
    private Player turn = Player.WHITE;
    private Player aiPlayer = null;

    @FXML
    void initialize() {
        drawBoard();

        anchor.widthProperty().addListener(e -> padChessBoard());
        anchor.heightProperty().addListener(e -> padChessBoard());
        boardSplitPane.getDividers().get(0).positionProperty().addListener(e -> padChessBoard());

        loadResources();
        newGame();
        drawBoardUnits();
        updateGameStatus("");
    }

    void initStage(Stage stage) {
        this.stage = stage;
        padChessBoard();
        timer.setCycleCount(Timeline.INDEFINITE);
        timer.getKeyFrames().add(new KeyFrame(Duration.seconds(1), event -> timeIncrease()));
        timer.playFromStart();
    }

    /**
     * Creates and add the panes used to represent the chess board in the grid
     */
    @FXML
    private void drawBoard() {
        String[] color = {"cream", "brown"};
        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                Pane p = new Pane();
                p.setOnMouseClicked(this::mouseClick);
                p.setOnMouseEntered(this::mouseHover);

                p.getStyleClass().add(color[(x + y) % 2]);
                grid.add(p, x, y);
                panes[x][y] = p;
            }
        }
    }

    /**
     * Loads the images used by the game to represent the Chess pieces
     */
    private void loadResources() {
        String[] unitKeys = {"B", "H", "K", "Q", "R", "P"};
        for (String uK : unitKeys) {
            images.put("B" + uK, new Image(getClass().getResourceAsStream("resources/B" + uK + ".png")));
            images.put("W" + uK, new Image(getClass().getResourceAsStream("resources/W" + uK + ".png")));
        }
    }

    /**
     * Draws the units on the board, first clearing any currently existing nodes, then drawing.
     */
    private void drawBoardUnits() {
        for (Pane[] p : panes) {
            for (Pane pane : p) {
                pane.getChildren().clear();
            }
        }

        for (Chessman unit : game.getAllUnits()) {
            String unitKey = unit.getOwner().getName().substring(0, 1) + unit.getUnitType().getDisplay();
            ImageView image = new ImageView(images.get(unitKey));
            int x = unit.getPosition().x - 1;
            int y = 8 - unit.getPosition().y;
            image.fitWidthProperty().bind(panes[x][y].widthProperty());
            image.fitHeightProperty().bind(panes[x][y].heightProperty());
            image.setSmooth(true);
            image.setCache(true);
            image.setPreserveRatio(true);
            image.setMouseTransparent(true);
            panes[x][y].getChildren().add(image);
        }
    }

    /**
     * Pads the grid to maintain the aspect ratio of the board when resizing and keep it centered
     */
    private void padChessBoard() {
        double horizontal = 0, vertical = 0;
        if (grid.getWidth() > grid.getHeight()) {
            double padding = grid.getWidth() - grid.getHeight();
            horizontal = padding / 2;
        } else {
            double padding = grid.getHeight() - grid.getWidth();
            vertical = padding / 2;
        }
        grid.setPadding(new Insets(vertical, horizontal, vertical, horizontal));
    }

    /**
     * Colors a square as the mouse hovers over it
     *
     * @param event MouseEvent raised when mouse hovers node
     */
    private void mouseHover(MouseEvent event) {
        if (hoveredPane != null) {
            hoveredPane.getStyleClass().remove("hovered");
        }
        hoveredPane = (Pane) event.getSource();
        hoveredPane.getStyleClass().add("hovered");
    }

    /**
     * Handles mouse clicks on pieces. Right Click deselects any currently selected node.
     * Left click first selects a node, then on choosing a second attempts to perform a move.
     *
     * @param event MouseEvent raised on click on node
     */
    private void mouseClick(MouseEvent event) {
        Pane source = (Pane) event.getSource();
        Pos target = getNodeGridPos(source);
        switch (event.getButton()) {
            case PRIMARY:
                if (selectedPane != null) {
                    Pos origin = getNodeGridPos(selectedPane);
                    if (target.x != origin.x || target.y != origin.y) {
                        Pos transformedOrigin = new Pos(origin.x + 1, 8 - origin.y);
                        Pos transformedTarget = new Pos(target.x + 1, 8 - target.y);
                        boolean good = game.moveUnit(transformedOrigin, transformedTarget, turn);
                        printLastMove();
                        if (good) {
                            moveUnitPane(source);
                            checkVictorStatus();
                            changeTurn();
                        } else {
                            updateGameStatus("Bad Move. ");
                        }
                    }
                    selectedPane.getStyleClass().remove("selected");
                    selectedPane = null;
                } else {
                    selectedPane = source;
                    selectedPane.getStyleClass().add("selected");
                }
                break;
            case SECONDARY:
                if (selectedPane != null) {
                    selectedPane.getStyleClass().remove("selected");
                    selectedPane = null;
                }
            default:
                break;
        }
    }

    /**
     * Gets the position of a node within the grid
     *
     * @param source node to find position of
     * @return coordinates of node
     */
    private Pos getNodeGridPos(Node source) {
        int x = GridPane.getColumnIndex(source);
        int y = GridPane.getRowIndex(source);
        return new Pos(x, y);
    }

    /**
     * Moves a unit (node) by first checking the target position is empty
     *
     * @param target place to move to
     */
    private void moveUnitPane(Pane target) {
        target.getChildren().clear();
        Node image = selectedPane.getChildren().remove(0);
        target.getChildren().add(image);
    }

    /**
     * Changes the turn of game whilst checking for stalemate
     */
    private void changeTurn() {
        if (turn.equals(Player.WHITE)) {
            turn = Player.BLACK;
        } else {
            turn = Player.WHITE;
        }
        updateGameStatus("");
        checkStaleMate();
        if (turn.equals(aiPlayer)) {
            doAutomaticMove();
        }
    }

    /**
     * Performs a move if AI is enabled
     */
    private void doAutomaticMove() {
        game.performAutomaticMove(aiPlayer);
        printLastMove();
        drawBoardUnits();
        checkVictorStatus();
        changeTurn();
    }

    /**
     * Print last move
     */
    private void printLastMove() {
        updateStatus(game.getLastMove());
    }

    /**
     * Checks for stalemate, creating a dialog if so
     */
    private void checkStaleMate() {
        if (game.stalemate(turn)) {
            endGameConfirmation("Stalemate!", turn.getName() + " can't move.", null);
        }
    }

    /**
     * Button used to activate and deactivate the AI
     */
    @FXML
    private void toggleAI() {
        if (aiPlayer == null) {
            aiPlayer = turn;
            doAutomaticMove();
        } else {
            aiPlayer = null;
        }
    }

    /**
     * Updates the game status with current turn
     *
     * @param status extra information
     */
    private void updateGameStatus(String status) {
        String buffer = "";
        buffer += status;
        buffer += turn.getName() + "'s turn. ";
        buffer += (game.getCheckedPlayer() != null ? game.getCheckedPlayer().getName() + " is in check! " : "");

        updateStatus(buffer);
    }

    /**
     * Updates the status only including the message and time
     *
     * @param status extra information
     */
    private void updateStatus(String status) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String buffer = System.lineSeparator() + "[";
        buffer += sdf.format(System.currentTimeMillis());
        buffer += "]  ";
        buffer += status;

        gameStatusLabel.appendText(buffer);
        gameStatusLabel.setScrollTop(Double.MAX_VALUE);
    }

    /**
     * Detects if a pawn is up for promotion or checkmate and acts appropriately
     */
    private void checkVictorStatus() {
        switch (game.victor()) {
            case PAWN_PROMOTION:
                while (true) {
                    ChoiceDialog<String> choice = new ChoiceDialog<>("1. Queen", "1. Queen", "2. Knight", "3. Rook", "4. Bishop");
                    choice.setTitle("Pawn Promotion");
                    choice.setHeaderText("Choose a unit: ");
                    choice.setGraphic(null);
                    Optional<String> result = choice.showAndWait();
                    if (result.isPresent()) {
                        game.promotePawn(Integer.parseInt(result.get().substring(0, 1)));
                        drawBoardUnits();
                        break;
                    }
                }
                break;
            case CHECKMATE:
                String player = game.getCheckedPlayer().getName();
                endGameConfirmation("Checkmate!", player + " is in checkmate!", images.get(player.substring(0, 1) + "K"));
                break;
        }
    }

    /**
     * Increases the timer for a player on there turn
     */
    private void timeIncrease() {
        playerSeconds[turn.getValue()] += 1;
        Format formatter = new SimpleDateFormat("mm:ss");
        String buffer = turn.getName() + ": " + formatter.format(new Date(playerSeconds[turn.getValue()] * 1000));
        switch (turn) {
            case WHITE:
                whiteTimerLabel.setText(buffer);
                break;
            case BLACK:
                blackTimerLabel.setText(buffer);
                break;
        }
    }

    /**
     * Creates an end game dialog
     *
     * @param title  String
     * @param header String
     * @param image  String
     */
    private void endGameConfirmation(String title, String header, Image image) {
        boolean response = promptConfirmation(title, header, "Would you like to start a new game?", image);
        if (response) {
            game = new Board(true);
            drawBoardUnits();
            return;
        }
        Platform.exit();
    }

    /**
     * Creates a confirmation dialog
     *
     * @param title   String
     * @param header  String
     * @param content String
     * @param image   Image
     * @return true if Yes
     */
    private boolean promptConfirmation(String title, String header, String content, Image image) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().setAll(images.get("BQ")); // Prone to crashing
        ImageView icon = new ImageView(image);
        icon.setFitHeight(50);
        icon.setFitWidth(50);
        alert.setGraphic(icon);

        ButtonType yes = new ButtonType("Yes");
        ButtonType no = new ButtonType("No");

        alert.getButtonTypes().setAll(yes, no);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent()) {
            if (result.get() == yes) {
                return true;
            }
        }
        return false;
    }

    /**
     * Creates a save file dialog to save the game
     */
    @FXML
    private void saveFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Game");
        fileChooser.setInitialDirectory(new File("").getAbsoluteFile());
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Text Files", "*.txt"),
                new FileChooser.ExtensionFilter("All Files", "*.*")
        );
        File file = fileChooser.showSaveDialog(stage);
        if (file != null) {
            String success = game.save(file.getAbsolutePath(), turn.getName());
            updateStatus(success);
        }
    }

    /**
     * Creates a load file dialog to load a game
     */
    @FXML
    private void loadFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load Game");
        fileChooser.setInitialDirectory(new File("").getAbsoluteFile());
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Text Files", "*.txt"),
                new FileChooser.ExtensionFilter("All Files", "*.*")
        );
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            String success = game.load(file.getAbsolutePath());
            drawBoardUnits();
            String currentTurn = success.substring(0, 5);
            if (currentTurn.equals(Player.WHITE.getName())) {
                turn = Player.WHITE;
            } else {
                turn = Player.BLACK;
            }
            updateStatus(success.substring(6, success.length()));
            updateGameStatus("");
        }
    }

    /**
     * Prompts the user for the settings for a new game
     */
    @FXML
    private void newGame() {
        turn = Player.WHITE;
        boolean fastGame = promptConfirmation("Chess", "Would you like to play a fast game?", "", null);
        if (fastGame) {
            game = new Board(false);
        } else {
            game = new Board(true);
        }

        boolean ai = promptConfirmation("Chess", "Would you like to vs an AI?", "", null);
        if (ai) {
            toggleButtonAI.setSelected(true);
            boolean side = promptConfirmation("Chess", "Would you like to play as White?", "", null);
            if (side) {
                aiPlayer = Player.BLACK;
            } else {
                aiPlayer = Player.WHITE;
                doAutomaticMove();
            }
        } else {
            aiPlayer = null;
        }
        drawBoardUnits();
    }
}
