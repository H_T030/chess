package chess.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Main Graphic User Interface class used for the initialisation of the game and GUI
 *
 * @author Harrison Towell
 * @version 1.0
 */
public class Main extends Application {

    /**
     * Starts javafx
     *
     * @param args console command arguments
     */
    public static void main(String[] args) {
        if (args.length > 0) {
            if (args[0].equals("textui")) {
                chess.textui.TextUI.main(args);
            }
        } else {
            launch(args);
        }
    }

    /**
     * Starts the gui and provides the controller with the stage
     *
     * @param stage main window
     */
    @Override
    public void start(Stage stage) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Chess.fxml"));
            Parent root = loader.load();
            Controller controller = loader.getController();


            Scene scene = new Scene(root, 600, 600);
            stage.setMinWidth(200);
            stage.setMinHeight(200);
            stage.getIcons().add(new Image(getClass().getResourceAsStream("resources/BQ.png")));
            stage.setTitle("Chess");
            stage.setScene(scene);
            stage.show();
            controller.initStage(stage);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
